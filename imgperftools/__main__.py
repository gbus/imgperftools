from imgperftools.configuration import ConfigurationReader
from imgperftools.testapp import TestApp
from os.path import expanduser

config_file = expanduser("~") + '/.imgperftools/config.yaml'


def main():
    '''Run the image conversion tools to test the performance'''

    # Read the configuration
    conf = ConfigurationReader(config_file)
    base_path, image_path, csv_path = conf.get_paths()
    resize = conf.get_resize()
    time_opts = conf.get_time_options()
    image_list = conf.get_images()

    # For each image tool run the performance test
    for name in conf.get_app_list():
        print("Testing {}".format(name))

        app = conf.get_app_config(name)

        ta = TestApp(name, app, resize, image_list, time_opts, base_path, image_path, csv_path)
        ta.run_test()
        ta.cleanup()

if __name__ == "__main__":
    main()