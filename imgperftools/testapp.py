import os, errno
from os.path import isfile, join, getsize
from sys import exit
import subprocess
import shutil
from PIL import Image

class TestApp():
    result_file = None

    def __init__(self, name, app_conf, resize, image_list, time_opts, base_path, image_path, result_path):
        self.name = name
        self.app_conf = app_conf
        self.resize = resize
        self.image_list = image_list
        self.time_opts_names, self.time_opts = time_opts.keys(), time_opts.values()
        self.base_path = base_path
        self.image_path = image_path
        self.result_path = result_path
        self.out_image_path = base_path + '/' + self.name   # Temp directory for output images
                                                            # named as the application name
        self._check_commands()
        self.tags = self._init_tags()
        self._mk_dirs()
        self.result_file = self._init_result_file()

    def _executable(self, command):
        '''Check if /full_path/command exists and can be executed'''
        ret = False
        if isfile(command):
            ret = os.access(command, os.X_OK)   # using os.path to test if the command is executable (X_OK)
        return ret

    def _check_commands(self):
        '''Check if the commands to be run are executable. If not notify the user and exit'''
        for p in '/usr/bin/time', self.app_conf['prog']:
            if not self._executable(p):
                print("The command \"{}\" can't be run. Ensure it is installed and can be executed".format(p))
                exit(-1)

    def _init_tags(self):
        '''Each image conversion tool has its own argument syntax.
        Starting from a template command, several tags get replaced to form the final command.
        This function supports 2 formats:
          - width x height
          - scaling factor
        These values are replaced in the template command together with in and out files'''

        # Check if the application accepts resize as WxH or as scaling factor
        rf = self.app_conf['resize_format']
        tags = {}
        if rf == 'F':       # Expect to pass a scaling factor
            tags['_F_'] = ''
        elif rf == 'WxH':   # Expect to pass parameters in the format width x height
            tags['_W_'] = str(self.resize['_W_'])
            tags['_H_'] = str(self.resize['_H_'])
        else:
            print("Error: wrong resize format in config file. It must be one of \"F\" or \"WxH\"")
            exit(-1)
        return tags

    def _mk_dirs(self):
        '''Create the directories if they don't exist'''
        for directory in self.base_path, self.image_path, self.result_path, self.out_image_path:
            try:
                os.makedirs(directory)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

    def _init_result_file(self):
        try:
            f = open("{}/{}.csv".format(self.result_path,self.name), "w")
            f.write("{}\t{}\t{}\t{}\n".format(
                "Filename",
                "size_in",
                "size_out",
                '\t'.join(self.time_opts_names)      # Resource usage names from config
                ))
        except IOError:
            print("Error: Result file can't be opened for writing")
            exit(-1)
        return f

    def _run_cmd(self, command):
        '''Run a command in a shell and return both stdout and stderr'''
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        return p.communicate()

    def _calc_scale_factor(self):
        '''Calculate the image scale factor as a ratio between wanted size and original size.'''
        orig_size = Image.open(self.pic_src).size
        w_ratio = self.resize['_W_'] / orig_size[0]
        h_ratio = self.resize['_H_'] / orig_size[1]
        return max( w_ratio, h_ratio)   # We want the max between width and height to be the converted size

    def _build_img_command(self):
        '''Replace tags in command template'''
        self.tags['_IN_FILE_'] = "\"{}\"".format(self.pic_src)
        self.tags['_OUT_FILE_'] = "\"{}\"".format(self.pic_dest)

        command = self.app_conf['cmd_template']
        for key in self.tags.keys():
            tag_val = self.tags[key]
            if key == '_F_':    # Dynamically calculate the scaling factor for this image
                tag_val = str(self._calc_scale_factor())
            command = command.replace(key, tag_val)
        return command

    def _assemble_time_command(self):
        '''Returns the time command followed by the image convert program to run'''
        return "/usr/bin/time -f \"{}\" {}".format(
            '\\t'.join(self.time_opts),       # Time format (tab delimited)
            self._build_img_command(),        # Command with tags replaced
        )

    def run_test(self):
        '''Run the graphic application and gather performance statistics'''

        for pic in self.image_list:
            self.pic_src = "{}/{}".format(self.image_path, pic)
            self.pic_dest = "{}/{}".format(self.out_image_path, pic)

            command = self._assemble_time_command()

            # Run the command and get the usage resources from stderr
            stdout, stderr = self._run_cmd(command)
            resource_usage = stderr.decode('utf-8')    # Remove trailing \n and deconding as string

            # Assemble the result line for the csv
            line = "{}\t{}\t{}\t{}".format(
                pic,                # Picture name
                getsize(self.pic_src),   # Size of input image
                getsize(self.pic_dest),  # Size of out image
                resource_usage      # Resource usage returned by time
            )

            self.result_file.write(line)
            self._display_progress()

        # Test complete. Finalize.
        self._display_progress(finalize=True)

    def _display_progress(self, finalize=False):
        '''Display a dot for each processed image. Print a newline if finalize = True'''
        if not finalize:
            print('.', end='', flush=True)
        else:
            print("\nDone.")

    def cleanup(self):
        '''Remove the generated images'''
        shutil.rmtree(self.out_image_path)

