import yaml
import os
import imghdr
from os.path import isfile, join

class ConfigurationReader:
    '''Read the configuration sections from the yaml configuration file'''
    configuration = {}
    image_list = []
    image_path = ''
    config_sections = ('base_path', 'image_path', 'csv_path', 'time_options', 'applications')

    def __init__(self, file = 'config.yaml'):
        self.config_file = file
        self._load_config()

    def _load_config(self):
        '''Open config_file and parse the yaml content'''
        try:
            f = open(self.config_file, 'r')
            self.configuration = yaml.load(f)
        except IOError:
            print("Error: Configuration file can't be opened. Ensure ~/.imgperftools/config.yaml is set")
            exit(-1)

        for section in self.config_sections:
            if section not in self.configuration:
                print("Error: Configuration not valid. Couldn't find the section {}".format(section))
                exit(-1)


    def get_app_list(self):
        '''Return the list of configured applications to test'''
        return self.configuration['applications'].keys()

    def get_app_config(self, name):
        '''Return an application configuration'''
        return self.configuration['applications'][name]

    def get_paths(self):
        '''Return the configured paths: base_path, input images path, and the output csv file path'''
        b_path = self.configuration['base_path']
        self.image_path = b_path + self.configuration['image_path']
        csv_path = b_path + self.configuration['csv_path']
        return b_path, self.image_path, csv_path

    def get_resize(self):
        '''Return the resize values as dict {_W_: width, _H_: height}'''
        return self.configuration['resize']

    def get_time_options(self):
        '''Return the list of configured time options'''
        return self.configuration['time_options']

    def get_images(self):
        '''Init a list of filenames as listed from the configured image directory'''
        image_list = []
        for f in os.listdir(self.image_path):
            curr_pic = join(self.image_path, f)
            if isfile(curr_pic):
                if imghdr.what(curr_pic) == 'jpeg':
                    image_list.append(f)
                else:
                    print("Warning: The file {} is not a jpeg. Skipping...".format(f))
        return image_list


