Image performance tools
-----------------------
Comparison tool for different image conversion programs.

Description
-----------
Given a list of images this tool runs different command line image conversion programs, one at a time.
The programs are tested for performance, by looking at the amount of computing resources  used and the execution time.
This is achieved by using the gnu command "time" (https://www.gnu.org/software/time/).
The information reported by "time" are by default:

    time_options:
      - "%e"    # Elapsed real time (in seconds).
      - "%S"    # Total number of CPU-seconds that the process spent in kernel mode.
      - "%U"    # Total number of CPU-seconds that the process spent in user mode.
      - "%M"    # Maximum resident set size of the process during its lifetime, in Kbytes.
      - "%I"    # Number of filesystem inputs by the process.
      - "%O"    # Number of filesystem outputs by the process.

More options can be added in config.yaml as documented in "man time".

For each tested program a csv file is generated. Each line reports the image file name, the original size, the size
of the converted file and the resource usage information returned by "time".

Installation
------------
First install the basic packages:
    sudo apt-get install git time python3-setuptools python3-yaml python3-pil

Install all the graphic packages that need to be tested, ie:
    sudo apt-get install imagemagick graphicsmagick exactimage

    git clone git@gitlab.com:gbus/imgperftools.git

    cd imgperftools

    sudo python3 setup.py install

Configuration
-------------
A config.yaml file needs to be copied under ~/.imgperftools/ :

    mkdir ~/.imgperftools

    cp ./config/config.yaml ~/.imgperftools

Edit config.yaml to (at least base_path needs to be specified):

- set paths (see comments in config.yaml)

- set the details of the programs to test (command line arguments).


Run the tests
-------------

Just run the following command:

img_perf_start

For each test the progress is displayed and any errors will stop the processing (exit code -1).
If the image directory contains a file of type different from jpeg a warning is displayed and it will be skipped.

When all tests are completed the csv files will be in <base_dir>/csv


Limitations
-----------

The application could be improved by implementing a better method to handle errors. 
The current version only does minimal checks, but it relies on well defined paths and a correct config file.
This also makes the configuration of the environment a little bit tedious for the end user.
