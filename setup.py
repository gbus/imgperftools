#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Setup script for imgperftools."""

from setuptools import setup
import io

version='0.3'

setup(
    name="imgperftools",
    version=version,
    packages=['imgperftools'],
    install_requires=['PyYaml>=3.12'],
    include_package_data=True,

    # metadata
    author="Gianluca Busiello",
    author_email="gianluca.busiello@gmail.com",
    description="Compare the performance of image conversion utilities",
    long_description=io.open('README.rst', 'r', encoding="UTF-8").read(),
    license="MIT",
    url="https://gitlab.com/gbus/imgperftools",   # project home page, if any
    project_urls={
        "Source Code": "https://gitlab.com/gbus/imgperftools",
    },

    entry_points = {
        'console_scripts': [
            'img_perf_start = imgperftools.__main__:main'
        ],
    },
    zip_safe=False
)